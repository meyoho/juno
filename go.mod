module alauda.io/juno

go 1.12

replace k8s.io/apimachinery => k8s.io/apimachinery v0.0.0-20190313205120-d7deff9243b1

replace k8s.io/apiserver => k8s.io/apiserver v0.0.0-20190606205144-71ebb8303503

replace k8s.io/api => k8s.io/api v0.0.0-20190612125737-db0771252981

replace helm.sh/helm => github.com/alauda/helm v3.0.0-alpha.1.0.20190923102049-fc6233ce9c16+incompatible

replace github.com/alauda/helm-crds => github.com/alauda/helm-crds v0.0.0-20200311040112-a1f7d31043c4

replace github.com/docker/docker => github.com/moby/moby v0.7.3-0.20190826074503-38ab9da00309

require (
	bitbucket.org/mathildetech/alauda-backend v0.1.24
	bitbucket.org/mathildetech/app v1.0.1
	bitbucket.org/mathildetech/log v1.0.5
	github.com/Jeffail/gabs/v2 v2.1.0
	github.com/alauda/captain v0.9.2
	github.com/alauda/helm-crds v0.0.0-20190915014518-6c1be05f7d6e
	github.com/emicklei/go-restful v2.10.0+incompatible
	github.com/gsamokovarov/assert v0.0.0-20180414063448-8cd8ab63a335
	github.com/juju/loggo v0.0.0-20190526231331-6e530bcce5d8 // indirect
	github.com/juju/testing v0.0.0-20190723135506-ce30eb24acd2 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pkg/errors v0.8.1
	github.com/spf13/pflag v1.0.3
	go.uber.org/zap v1.10.0
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22 // indirect
	helm.sh/helm v3.0.0-alpha.1.0.20190613170622-c35dbb7aabf8+incompatible
	k8s.io/api v0.0.0-20191010143144-fbf594f18f80
	k8s.io/apimachinery v0.0.0-20191006235458-f9f2f3f8ab02
	k8s.io/client-go v11.0.1-0.20190409021438-1a26190bd76a+incompatible
	k8s.io/klog v1.0.0
)
