package main

import (
	"alauda.io/juno/cmd/juno/app"
	"math/rand"
	"os"
	goruntime "runtime"
	"time"
)

// main func only inits app and Run
// these is a `bitbucket.org/mathildetech/app` feature
func main() {

	//flag.Parse()
	//FixKlogFlags()

	rand.Seed(time.Now().UTC().UnixNano())
	if len(os.Getenv("GOMAXPROCS")) == 0 {
		goruntime.GOMAXPROCS(goruntime.NumCPU())
	}
	app.NewApp("juno").Run()
}
