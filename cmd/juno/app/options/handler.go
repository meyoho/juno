package options

import (
	"alauda.io/juno/pkg/api"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"github.com/spf13/pflag"
)

type AppCatalogOptions struct {
}

func (o *AppCatalogOptions) AddFlags(fs *pflag.FlagSet) {
	return
}

func (o *AppCatalogOptions) ApplyFlags() []error {
	return nil
}

func (o *AppCatalogOptions) ApplyToServer(srv server.Server) error {
	h := api.NewHandler()
	return h.ApplyToServer(srv)
}

func NewAppCatalogOptions() *AppCatalogOptions {
	return &AppCatalogOptions{}
}
