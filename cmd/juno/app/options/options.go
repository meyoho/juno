package options

import (
	"bitbucket.org/mathildetech/alauda-backend/pkg/server/options"
)

// Options options for alauda-console
type Options struct {
	options.Optioner
}

// NewOptions new options for alauda-console
func NewOptions() *Options {
	// recommended := options.NewRecommendedOptions().Options
	return &Options{
		Optioner: options.With(
			NewAppCatalogOptions(),
			NewRecommendedOptions(),
		),
	}
}

// NewRecommendedOptions constructor for recommended options used on the server
func NewRecommendedOptions() *options.RecommendedOptions {
	return &options.RecommendedOptions{
		Options: options.With(
			options.NewLogOptions(),
			// opt.NewKlogOptions(),
			options.NewInsecureServingOptions(),
			options.NewClientOptions(),
			options.NewDebugOptions(),
			options.NewMetricsOptions(),
			options.NewAPIRegistryOptions(),
			options.NewOpenAPIOptions(),
			options.NewErrorOptions(),
		),
	}
}
