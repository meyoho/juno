package app

import (
	"alauda.io/juno/cmd/juno/app/options"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"bitbucket.org/mathildetech/app"
)

// `bitbucket.org/mathildetech/app` creator wrapper
// options are recommended options provided by the framework that
// if custimezed options are necessary, please check pkg/server/options.Optioner interface
func NewApp(name string) *app.App {
	opts := options.NewOptions()
	// app name nad description
	application := app.NewApp("Application Catalog Service", name,
		app.WithOptions(opts),
		app.WithDescription("Application Catalog Advanced API Service"),
		app.WithRunFunc(run(opts)),
	)
	return application
}

// wire up your own webservice
func run(opts *options.Options) app.RunFunc {
	return func(basename string) error {
		s := server.New(basename)
		if err := opts.ApplyToServer(s); err != nil {
			return err
		}
		s.Start()
		return nil
	}
}
