package wrapper

import (
	"bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	stdcontext "context"
	"github.com/alauda/helm-crds/pkg/client/clientset/versioned"
	"github.com/emicklei/go-restful"
	"k8s.io/client-go/rest"
)

type contextKey struct{ Name string }

var (
	restConfigKey = contextKey{Name: "rest.Config"}

	// This was added because we need a way to do unit test
	appClientKey = contextKey{Name: "rest.AppClient"}
)

// WithConfig inserts a config into the context
func WithConfig(ctx stdcontext.Context, config *rest.Config) stdcontext.Context {
	return stdcontext.WithValue(ctx, restConfigKey, config)
}

func WithAppClient(ctx stdcontext.Context, client versioned.Interface) stdcontext.Context {
	return stdcontext.WithValue(ctx, appClientKey, client)
}

type Client struct {
	decorator.Client
}

func NewClient(srv server.Server) Client {
	return Client{decorator.NewClient(srv)}
}

// InsecureFilter filter to populate the context with a ServiceAccount client
// will handle errors and may return errors on request
// TODO: replace it with ConfigAndClientFilter
func (d Client) ConfigFilter(req *restful.Request, res *restful.Response, chain *restful.FilterChain) {
	config, err := d.GetManager().Config(req)
	d.SetConfigContext(config, err, req, res, chain)
}

// ConfigAndClientFilter set config and client to context
func (d Client) ConfigAndClientFilter(req *restful.Request, res *restful.Response, chain *restful.FilterChain) {
	config, err := d.GetManager().Config(req)
	d.setConfigAndAppClientContext(config, err, req, res, chain)
}

// setConfigAndAppClientContext set config and app client to context
func (d Client) setConfigAndAppClientContext(config *rest.Config, err error, req *restful.Request, res *restful.Response, chain *restful.FilterChain) {
	if err != nil || config == nil {
		d.Server.HandleError(err, req, res)
		return
	}

	client, err := versioned.NewForConfig(config)
	if err != nil {
		d.Server.HandleError(err, req, res)
		return
	}
	req.Request = req.Request.WithContext(WithConfig(req.Request.Context(), config))
	req.Request = req.Request.WithContext(WithAppClient(req.Request.Context(), client))
	chain.ProcessFilter(req, res)
}

func (d Client) SetConfigContext(config *rest.Config, err error, req *restful.Request, res *restful.Response, chain *restful.FilterChain) {
	if err != nil || config == nil {
		d.Server.HandleError(err, req, res)
		return
	}
	req.Request = req.Request.WithContext(WithConfig(req.Request.Context(), config))
	chain.ProcessFilter(req, res)
}

// Client fetches a client from a context if existing.
// will return nil if the context doesnot have the client
func Config(ctx stdcontext.Context) *rest.Config {
	val := ctx.Value(restConfigKey)
	if val != nil {
		return val.(*rest.Config)
	}
	return nil
}

// GetClient get client from context, return nil if not found or type assert error
func GetClient(ctx stdcontext.Context) versioned.Interface {
	val := ctx.Value(appClientKey)
	if val != nil {
		return val.(versioned.Interface)
	}
	return nil
}
