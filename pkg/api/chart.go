package api

import (
	"alauda.io/juno/pkg/helm"
	"alauda.io/juno/pkg/wrapper"
	"bitbucket.org/mathildetech/log"
	"encoding/json"
	"fmt"
	"github.com/alauda/helm-crds/pkg/apis/app/v1alpha1"
	clientset "github.com/alauda/helm-crds/pkg/client/clientset/versioned"
	"github.com/emicklei/go-restful"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"strings"
)

// GetChartVersion get specific version for a chart
func getChartVersion(client clientset.Interface, ns, name, version string) (*v1alpha1.ChartVersion, error) {

	chart, err := client.AppV1alpha1().Charts(ns).Get(name, metav1.GetOptions{})
	if err != nil {
		logger.Error("get chart error", log.Err(err))
		return nil, err
	}

	var cr *v1alpha1.ChartVersion
	if version == "_latest" {
		cr = chart.Spec.Versions[0]
	} else {
		for _, item := range chart.Spec.Versions {
			if item.Version == version {
				cr = item
			}
		}
	}

	if cr == nil {
		logger.Error("get chart version error, not found")
		err := apierrors.NewNotFound(
			schema.GroupResource{
				Group:    "app.alauda.io",
				Resource: "ChartVersion",
			},
			name,
		)
		return nil, err
	}

	return cr, nil

}

// GetChartFiles retrieve a chart package and parse it's files
func (h *Handler) GetChartFiles(req *restful.Request, res *restful.Response) {
	ns := req.PathParameter("namespace")
	name := req.PathParameter("name")
	version := req.PathParameter("version")
	cfg := wrapper.Config(req.Request.Context())

	client, err := getAppClient(req)
	if err != nil {
		logger.Error("create client error", log.Err(err))
		h.Server.HandleError(err, req, res)
		return
	}

	c := ComposedClient{
		cfg:       cfg,
		appClient: client,
	}

	cr, err := getChartVersion(client, ns, name, version)
	if err != nil {
		h.Server.HandleError(err, req, res)
		return
	}

	repo, err := c.getRepoInfo(ns, name)
	if err != nil {
		logger.Error("get models info error", log.Err(err))
		h.Server.HandleError(err, req, res)
		return
	}

	files, err := helm.DownloadChart(repo, cr.URLs[0])
	if err != nil {
		logger.Error("download chart error", log.Err(err))
		h.Server.HandleError(err, req, res)
		return
	}

	res.WriteAsJson(files)

	return
}

// listCharts get all charts and add info about it's related app
// .... too many args.
// the two return args should be merged into one.
func (c *ClusterAppClient) listCharts(appNamespace, appCluster, ns, labelSelector, fieldSelector string) (runtime.Object, error) {
	client := c.client

	opts := metav1.ListOptions{
		LabelSelector: labelSelector,
		FieldSelector: fieldSelector,
	}

	logger.Debug("query charts with selector", log.Any("opts", opts))

	charts, err := client.AppV1alpha1().Charts(ns).List(opts)
	if err != nil {
		logger.Error("list charts error", log.Err(err))
		return nil, err
	}

	if appNamespace == "" {
		return charts, nil
	}

	if appCluster != "" {
		if err := c.setClientForCluster(appCluster); err != nil {
			logger.Error("init client for cluster error", log.String("name", appCluster))
			return nil, err
		}
		client = c.clients[appCluster]
	}

	apps, err := client.AppV1alpha1().HelmRequests(appNamespace).List(metav1.ListOptions{})
	if err != nil {
		logger.Error("list charts error", log.Err(err))
		return nil, err
	}

	logger.Debug("get apps from cluster", log.Int("count", len(apps.Items)))

	m := make(map[string]string)
	for _, item := range apps.Items {
		ss := strings.Split(item.Spec.Chart, "/")
		m[fmt.Sprintf("%s.%s", ss[1], ss[0])] = item.GetName()
	}

	logger.Info("app/chart map:", log.Any("map", m))

	var l unstructured.UnstructuredList
	data, _ := json.Marshal(charts)
	json.Unmarshal(data, &l)

	for _, item := range l.Items {
		unstructured.SetNestedField(item.Object, m[item.GetName()], "spec", "appName")
	}
	//l.SetAPIVersion(charts.APIVersion)
	//l.SetKind(charts.Kind)
	l.Object = nil

	return &l, nil
}

// ugly code
func (h *Handler) ListCharts(req *restful.Request, res *restful.Response) {
	c := NewClusterAppClient(wrapper.Config(req.Request.Context()))
	client, err := getAppClient(req)
	if err != nil {
		logger.Error("create client error", log.Err(err))
		h.Server.HandleError(err, req, res)
		return
	}
	c.client = client

	result, err := c.listCharts(
		req.QueryParameter("appNamespace"),
		req.QueryParameter("appCluster"),
		req.PathParameter("namespace"),
		req.QueryParameter("labelSelector"),
		req.QueryParameter("fieldSelector"),
	)
	if err != nil {
		logger.Error("list charts error", log.Err(err))
		h.Server.HandleError(err, req, res)
		return
	}

	res.WriteAsJson(result)

	return
}
