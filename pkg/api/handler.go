package api

import (
	"alauda.io/juno/pkg/models"
	"alauda.io/juno/pkg/wrapper"
	"bitbucket.org/mathildetech/alauda-backend/pkg/decorator"
	"bitbucket.org/mathildetech/alauda-backend/pkg/server"
	"github.com/alauda/helm-crds/pkg/apis/app/v1alpha1"
	"github.com/emicklei/go-restful"
	"go.uber.org/zap"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/klog"
	"net/http"
)

var logger *zap.Logger

type Handler struct {
	Server server.Server
}

func NewHandler() *Handler {
	return &Handler{}
}

func (h *Handler) ApplyToServer(srv server.Server) error {
	h.Server = srv

	ws := decorator.NewWSGenerator().New(srv)
	ws.Path("/catalog/v1")
	ws.Doc("advanced api")

	logger = srv.L().Named("handler")

	// this is a Filter (middleware) generator and can provide a few
	// useful models to create a kubernetes.Interface
	clientMW := wrapper.NewClient(srv)

	ws.Route(
		ws.POST("chartrepos").
			Filter(clientMW.ConfigFilter).
			Reads(models.ChartRepoCreate{}).
			To(h.CreateChartRepo).
			Writes(v1alpha1.ChartRepo{}).
			Returns(http.StatusCreated, "OK", v1alpha1.ChartRepo{}),
	)

	ws.Route(
		ws.PUT("chartrepos/{namespace}/{name}").
			Filter(clientMW.ConfigFilter).
			Param(ws.PathParameter("namespace", "namespace").DataType("string")).
			Param(ws.PathParameter("name", "models name").DataType("string")).
			Reads(models.ChartRepoUpdate{}).
			To(h.UpdateChartRepo).
			Writes(v1alpha1.ChartRepo{}).
			Returns(http.StatusOK, "OK", v1alpha1.ChartRepo{}),
	)

	ws.Route(
		ws.PUT("chartrepos/{namespace}/{name}/resync").
			Filter(clientMW.ConfigFilter).
			Param(ws.PathParameter("namespace", "namespace").DataType("string")).
			Param(ws.PathParameter("name", "models name").DataType("string")).
			To(h.ResyncChartRepo).
			Writes(v1alpha1.ChartRepo{}).
			Returns(http.StatusOK, "OK", v1alpha1.ChartRepo{}),
	)

	ws.Route(
		ws.GET("/clusters/{cluster}/helmrequests/{namespace}/{name}/resources").
			Filter(clientMW.ConfigFilter).
			Param(ws.PathParameter("cluster", "cluster name").DataType("string")).
			Param(ws.PathParameter("namespace", "namespace").DataType("string")).
			Param(ws.PathParameter("name", "helmrequest name").DataType("string")).
			To(h.GetHelmRequestResources).
			Writes(models.ChartResources{}).
			Returns(http.StatusOK, "OK", models.ChartResources{}),
	)

	ws.Route(
		ws.GET("/charts/{namespace}").
			Filter(clientMW.ConfigAndClientFilter).
			Param(ws.PathParameter("namespace", "namespace").DataType("string")).
			Param(
				restful.QueryParameter("labelSelector", "label selector").DataType("string").Required(false),
			).
			Param(
				restful.QueryParameter("fieldSelector", "field selector").DataType("string").Required(false),
			).
			Param(
				restful.QueryParameter("appNamespace", "app namespace to be filtered to").DataType("string").Required(false),
			).
			Param(
				restful.QueryParameter("appCluster", "app cluster to be filtered to").DataType("string").Required(false),
			).
			Writes(unstructured.UnstructuredList{}).
			To(h.ListCharts).
			Returns(http.StatusOK, "OK", unstructured.UnstructuredList{}),
	)

	ws.Route(
		ws.GET("/charts/{namespace}/{name}/versions/{version}/files").
			Filter(clientMW.ConfigAndClientFilter).
			Param(ws.PathParameter("namespace", "namespace").DataType("string")).
			Param(ws.PathParameter("name", "chart name").DataType("string")).
			Param(ws.PathParameter("version", "chart version").DataType("string")).
			To(h.GetChartFiles).
			Writes(models.ChartFiles{}).
			Returns(http.StatusOK, "OK", models.ChartFiles{}),
	)
	srv.Container().Add(ws)
	//logger.Info("routes are:", log.Any("routes", ws.Routes()[2]))
	klog.Info("routes are: ", ws.Routes())
	return nil
}
