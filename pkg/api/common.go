package api

import (
	"alauda.io/juno/pkg/models"
	"alauda.io/juno/pkg/wrapper"
	"github.com/alauda/helm-crds/pkg/client/clientset/versioned"
	"github.com/emicklei/go-restful"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

// ClusterAppClient contains clients for multi cluster
type ClusterAppClient struct {
	// the origin rest config
	cfg *rest.Config
	// ....messy.
	client  versioned.Interface
	cfgs    map[string]*rest.Config
	clients map[string]versioned.Interface
}

func NewClusterAppClient(cfg *rest.Config) *ClusterAppClient {
	return &ClusterAppClient{
		cfg:     cfg,
		cfgs:    make(map[string]*rest.Config),
		clients: make(map[string]versioned.Interface),
	}
}

// setClientForCluster set config and client
func (c *ClusterAppClient) setClientForCluster(cluster string) error {
	_, ok := c.clients[cluster]
	if ok {
		return nil
	}
	cfg := models.ConfigForCluster(c.cfg, cluster)
	cc, err := versioned.NewForConfig(cfg)
	if err != nil {
		return err
	}
	c.cfgs[cluster] = cfg
	c.clients[cluster] = cc
	return nil
}

//  ComposedClient contains app client and core client, cfgf and appClient should always be inited.
type ComposedClient struct {
	cfg        *rest.Config
	coreClient kubernetes.Interface
	appClient  versioned.Interface
}

// setCoreClient init core client if not
func (c *ComposedClient) setCoreClient() error {
	if c.coreClient != nil {
		return nil
	}
	cc, err := kubernetes.NewForConfig(c.cfg)
	if err != nil {
		return err
	}
	c.coreClient = cc
	return nil
}

// getAppClient get app client from a request.
// this can handler context with client or only config
func getAppClient(req *restful.Request) (versioned.Interface, error) {
	client := wrapper.GetClient(req.Request.Context())
	if client != nil {
		return client, nil
	}

	cfg := wrapper.Config(req.Request.Context())
	return versioned.NewForConfig(cfg)
}

func getComposedClient(req *restful.Request) (*ComposedClient, error) {
	client, err := getAppClient(req)
	if err != nil {
		return nil, err
	}

	c := ComposedClient{
		cfg:       wrapper.Config(req.Request.Context()),
		appClient: client,
	}
	return &c, nil
}
