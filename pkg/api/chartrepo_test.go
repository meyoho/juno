package api

import (
	"alauda.io/juno/pkg/models"
	"encoding/json"
	"github.com/alauda/helm-crds/pkg/apis/app/v1beta1"
	appFake "github.com/alauda/helm-crds/pkg/client/clientset/versioned/fake"
	"github.com/gsamokovarov/assert"
	"go.uber.org/zap"
	"io/ioutil"
	v1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/fake"
	"testing"
)

func loadChartRepo(path string) *v1beta1.ChartRepo {
	plan, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	var data v1beta1.ChartRepo
	if err := json.Unmarshal(plan, &data); err != nil {
		panic(err)
	}
	return &data
}

func loadSecret(path string) *v1.Secret {
	plan, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	var data v1.Secret
	if err := json.Unmarshal(plan, &data); err != nil {
		panic(err)
	}
	return &data
}

func TestGetChartRepo(t *testing.T) {
	logger = zap.NewExample()

	ctr := loadChartRepo("../../test/fixtures/chartrepo.json")
	secret := loadSecret("../../test/fixtures/chartrepo-secret.json")

	c := ComposedClient{
		coreClient: fake.NewSimpleClientset(secret),
		appClient:  appFake.NewSimpleClientset(ctr),
	}
	result, err := c.getRepoInfo("alauda-system", "aaa.rook-test")
	assert.Nil(t, err)
	assert.Equal(t, result.URL, ctr.Spec.URL)
	assert.Equal(t, result.Password, "2wb4E1iydRj7")

}

func TestResyncChartRepo(t *testing.T) {
	logger = zap.NewExample()
	ctr := loadChartRepo("../../test/fixtures/chartrepo.json")
	c := ComposedClient{appClient: appFake.NewSimpleClientset(ctr)}
	result, err := c.markChartRepoPending("alauda-system", "rook-test")
	assert.Nil(t, err)
	assert.Equal(t, result.Status.Phase, "Pending")

}

func loadChartRepoUpdate(path string) *models.ChartRepoUpdate {
	plan, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	var data models.ChartRepoUpdate
	if err := json.Unmarshal(plan, &data); err != nil {
		panic(err)
	}
	return &data
}

func TestCreateChartRepo(t *testing.T) {
	logger = zap.NewExample()
	c := ComposedClient{
		appClient:  appFake.NewSimpleClientset(),
		coreClient: fake.NewSimpleClientset(),
	}
	body := models.ChartRepoCreate(*loadChartRepoUpdate("../../test/fixtures/update-chartrepo.json"))

	result, err := c.createChartRepo(&body)
	assert.Nil(t, err)
	assert.NotNil(t, result)
	_, err = c.coreClient.CoreV1().Secrets(body.GetNamespace()).Get(body.Spec.Secret.GetName(), metav1.GetOptions{})
	assert.Nil(t, err)

}

func TestUpdateChartRepo(t *testing.T) {
	logger = zap.NewExample()
	ctr := loadChartRepo("../../test/fixtures/chartrepo.json")
	s := loadSecret("../../test/fixtures/chartrepo-secret.json")
	c := ComposedClient{
		appClient:  appFake.NewSimpleClientset(ctr),
		coreClient: fake.NewSimpleClientset(s),
	}
	body := loadChartRepoUpdate("../../test/fixtures/update-chartrepo.json")
	// normal update, update url
	result, err := c.updateChartRepo("alauda-system", "rook-test", body)
	assert.Nil(t, err)
	assert.Equal(t, result.Spec.URL, body.Spec.ChartRepo.Spec.URL)

	// update with no secret
	// should we add validate???
	logger.Info("test --- 2 ")
	body.Spec.Secret = nil
	body.Spec.ChartRepo.Spec.Secret = nil
	result, err = c.updateChartRepo("alauda-system", "rook-test", body)
	assert.Nil(t, err)
	_, err = c.coreClient.CoreV1().Secrets(s.GetNamespace()).Get(s.GetName(), metav1.GetOptions{})
	assert.True(t, apierrors.IsNotFound(err))

}
