package api

import (
	"alauda.io/juno/pkg/models"
	"alauda.io/juno/pkg/wrapper"
	"bitbucket.org/mathildetech/log"
	"github.com/alauda/captain/pkg/release/storagedriver"
	"github.com/emicklei/go-restful"
	"helm.sh/helm/pkg/storage"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/klog"
)

// getHelmRequestResources get resources list from a release.
func (c *ClusterAppClient) getHelmRequestResources(cluster, ns, name string) (*models.ChartResources, error) {
	if err := c.setClientForCluster(cluster); err != nil {
		logger.Error("init client for cluster error", log.Err(err))
		return nil, err
	}

	client := c.clients[cluster]
	hr, err := client.AppV1alpha1().HelmRequests(ns).Get(name, metav1.GetOptions{})
	if err != nil {
		logger.Error("get hr error", log.Err(err))
		return nil, err
	}

	relName := hr.Spec.ReleaseName
	if relName == "" {
		relName = hr.Name
	}

	if hr.Spec.ClusterName != cluster && hr.Spec.ClusterName != "" {
		if err := c.setClientForCluster(hr.Spec.ClusterName); err != nil {
			logger.Error("init client for cluster error", log.Err(err))
			return nil, err
		}
		client = c.clients[hr.Spec.ClusterName]
	}

	d := storagedriver.NewReleases(client.AppV1alpha1().Releases(ns))
	d.Log = klog.Infof
	store := storage.Init(d)

	rel, err := store.Deployed(relName)
	if err != nil {
		logger.Error("get hr error", log.Err(err))
		return nil, err
	}

	rs, err := models.ParseChartResources(rel.Manifest)
	if err != nil {
		logger.Error("parse release manifest error", log.Err(err))
		return nil, err
	}

	return rs, nil
}

func (h *Handler) GetHelmRequestResources(req *restful.Request, res *restful.Response) {
	cfg := wrapper.Config(req.Request.Context())
	ns := req.PathParameter("namespace")
	name := req.PathParameter("name")
	cluster := req.PathParameter("cluster")

	c := NewClusterAppClient(cfg)
	rs, err := c.getHelmRequestResources(cluster, ns, name)
	if err != nil {
		logger.Error("get release manifest error", log.Err(err))
		h.Server.HandleError(err, req, res)
		return
	}

	res.WriteAsJson(rs)
	return

}
