package api

import (
	"encoding/json"
	"github.com/alauda/helm-crds/pkg/apis/app/v1alpha1"
	"github.com/alauda/helm-crds/pkg/client/clientset/versioned"
	"github.com/alauda/helm-crds/pkg/client/clientset/versioned/fake"
	"github.com/gsamokovarov/assert"
	"go.uber.org/zap"
	"io/ioutil"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"testing"
)

func loadChart(path string) *v1alpha1.Chart {
	plan, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	var data v1alpha1.Chart
	if err := json.Unmarshal(plan, &data); err != nil {
		panic(err)
	}
	return &data
}

func TestListCharts(t *testing.T) {
	logger = zap.NewExample()
	chart := loadChart("../../test/fixtures/chart.json")
	hr := loadHelmRequest("../../test/fixtures/hr.json")
	c := ClusterAppClient{
		client: fake.NewSimpleClientset(chart),
		clients: map[string]versioned.Interface{
			"bus": fake.NewSimpleClientset(),
		},
	}

	result, err := c.listCharts("", "", "alauda-system", "repo=test", "")
	assert.Nil(t, err)
	assert.NotNil(t, result)
	charts, ok := result.(*v1alpha1.ChartList)
	assert.True(t, ok)
	assert.Equal(t, chart.GetName(), charts.Items[0].GetName())

	// with app filters
	hr.Spec.Chart = "test/zetcd"
	c.clients["test"] = fake.NewSimpleClientset(hr)
	result, err = c.listCharts("alauda-system", "test", "alauda-system", "repo=test", "")
	assert.Nil(t, err)
	cl, ok := result.(*unstructured.UnstructuredList)
	assert.True(t, ok)
	assert.Equal(t, chart.GetName(), cl.Items[0].GetName())
	name, ok, err := unstructured.NestedString(cl.Items[0].Object, "spec", "appName")
	assert.Nil(t, err)
	assert.True(t, ok)
	assert.Equal(t, name, hr.GetName())

}

// API: GET /<prefix>/charts/{namespace}/{name}/versions/{version}
func TestGetChartVersion(t *testing.T) {
	t.Parallel()

	object := loadChart("../../test/fixtures/chart.json")
	client := fake.NewSimpleClientset(object)
	logger = zap.NewExample()

	versions := []struct{ version string }{
		{"_latest"},
		{"0.1.8"},
		{"not-exist"},
	}

	for _, tt := range versions {
		tt := tt
		t.Run(tt.version, func(t *testing.T) {
			t.Parallel()
			result, err := getChartVersion(client, "alauda-system", "zetcd.test", tt.version)
			if tt.version == "not-exist" {
				assert.Nil(t, result)
				assert.True(t, apierrors.IsNotFound(err))
			} else {
				assert.Nil(t, err)
				if tt.version == "_latest" {
					assert.True(t, result.Digest == object.Spec.Versions[0].Digest)
				}
				if tt.version == "0.1.8" {
					assert.True(t, result.Digest == "82bfecb0980eefaaa30361f632d8b04df5d1c0588d7aef69a52d825b3c68bea6")
				}
			}
		})
	}

}
