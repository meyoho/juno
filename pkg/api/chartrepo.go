package api

import (
	"fmt"
	"strings"

	util "alauda.io/juno/pkg/kubernetes"
	"alauda.io/juno/pkg/models"
	"alauda.io/juno/pkg/wrapper"
	"bitbucket.org/mathildetech/log"
	"github.com/Jeffail/gabs/v2"
	"github.com/alauda/helm-crds/pkg/apis/app/v1beta1"
	"github.com/emicklei/go-restful"
	"helm.sh/helm/pkg/repo"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
)

// getRepoSecret get secret for a repo
// rbac issues?
func (c *ComposedClient) getRepoSecret(ns, name string) (string, string, error) {
	logger.Info("need to get secret for models")
	if c.coreClient == nil {
		if err := c.setCoreClient(); err != nil {
			return "", "", err
		}
	}

	secret, err := c.coreClient.CoreV1().Secrets(ns).Get(name, metav1.GetOptions{})
	if err != nil {
		return "", "", nil
	}
	data := secret.Data
	return string(data["username"]), string(data["password"]), nil
}

func (c *ComposedClient) getRepoInfo(ns, chartName string) (*repo.Entry, error) {
	name := strings.Split(chartName, ".")[1]

	cr, err := c.appClient.AppV1beta1().ChartRepos(ns).Get(name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	entry := repo.Entry{
		URL: cr.Spec.URL,
	}

	if cr.Spec.Secret != nil {
		username, password, err := c.getRepoSecret(cr.GetNamespace(), cr.Spec.Secret.Name)
		if err != nil {
			return nil, err
		}
		entry.Username = username
		entry.Password = password

	}
	return &entry, nil
}

// markChartRepoPending mark a chartrepo as pending and wait for the controller to resync...
// do this really work?
func (c *ComposedClient) markChartRepoPending(ns, name string) (*v1beta1.ChartRepo, error) {
	data := gabs.New()
	data.SetP(v1beta1.ChartRepoPending, "status.phase")
	// data.SetP(rand.String(13), "metadata.annotations.resync-rand")
	result, err := c.appClient.AppV1beta1().ChartRepos(ns).Patch(name, types.MergePatchType, data.Bytes())
	if err != nil {
		logger.Error("update chatrepo error", log.Err(err))
		return nil, err
	}
	logger.Info("update chartrepo:", log.String("repo", result.GetName()))
	return result, err
}

func (h *Handler) ResyncChartRepo(req *restful.Request, res *restful.Response) {
	client, err := getComposedClient(req)
	if err != nil {
		logger.Error("create client error", log.Err(err))
		h.Server.HandleError(err, req, res)
		return
	}

	result, err := client.markChartRepoPending(req.PathParameter("namespace"), req.PathParameter("name"))
	if err != nil {
		logger.Error("update models error", log.Err(err))
		h.Server.HandleError(err, req, res)
		return
	}
	res.WriteAsJson(result)
	return
}

// updateChartRepo ... get and update
func (c *ComposedClient) updateChartRepoResource(ns, name string, body *models.ChartRepoUpdate) (*v1beta1.ChartRepo, *v1beta1.ChartRepo, error) {
	client := c.appClient

	old, err := client.AppV1beta1().ChartRepos(ns).Get(name, metav1.GetOptions{})
	if err != nil {
		logger.Error("get old chart repo error", log.Err(err))
		return nil, nil, nil
	}

	body.Spec.ChartRepo.SetResourceVersion(old.GetResourceVersion())
	body.Spec.ChartRepo.Status.Phase = v1beta1.ChartRepoPending

	result, err := client.AppV1beta1().ChartRepos(ns).Update(body.Spec.ChartRepo)
	if err != nil {
		logger.Error("create repo error", log.Err(err))
		return nil, nil, nil
	}
	logger.Info("update chartrepo", log.String("models", result.GetName()))
	return old, result, err
}

// syncChartRepoSecret secrets go and come...
// if body have secret, create/update it
// if body have no secret, but old repo have secret, remote the secret
func (c *ComposedClient) syncChartRepoSecret(body *models.ChartRepoUpdate, old *v1beta1.ChartRepo, new *v1beta1.ChartRepo) error {
	if body.Spec.Secret != nil {
		// logger.Debug("both sides secret exist, do update.")
		if err := c.updateChartRepoSecret(body, new); err != nil {
			logger.Error("update secret error", log.Err(err))
			return err
		}
	} else {
		// logger.Debug("update chartrepo body with no secret")
		// check if this repo remove auth info or not
		if old.Spec.Secret != nil {
			// logger.Debug("update chartrepo with no secret, remove the old one")
			if err := c.removeChartRepoSecret(new.GetNamespace(), old.Spec.Secret.Name); err != nil {
				logger.Error("remove secret error", log.Err(err))
				return err
			}
		}
	}
	return nil
}

// UpdateChartRepo update a ChartRepo
// This api mainly handle secret issues, shit code
func (h *Handler) UpdateChartRepo(req *restful.Request, res *restful.Response) {
	cfg := wrapper.Config(req.Request.Context())
	ns := req.PathParameter("namespace")
	name := req.PathParameter("name")

	logger.Debug("rest config is: ", log.Any("config", cfg))

	var body models.ChartRepoUpdate
	if err := req.ReadEntity(&body); err != nil {
		logger.Error("read request body error", log.Err(err))
		h.Server.HandleError(err, req, res)
		return
	}

	// validate
	if body.Spec.ChartRepo != nil && body.Spec.ChartRepo.Spec.Secret != nil && body.Spec.Secret == nil {
		err := apierrors.NewBadRequest("missing secret object when update chartrepo")
		logger.Error("request body validate error", log.Err(err))
		h.Server.HandleError(err, req, res)
		return
	}

	client, err := getComposedClient(req)
	if err != nil {
		logger.Error("create client error", log.Err(err))
		h.Server.HandleError(err, req, res)
		return
	}

	result, err := client.updateChartRepo(ns, name, &body)
	if err != nil {
		logger.Error("create repo error", log.Err(err))
		h.Server.HandleError(err, req, res)
		return
	}

	res.WriteAsJson(result)
	return
}

func (c *ComposedClient) updateChartRepo(ns, name string, body *models.ChartRepoUpdate) (*v1beta1.ChartRepo, error) {
	old, result, err := c.updateChartRepoResource(ns, name, body)
	if err != nil {
		logger.Error("create repo error", log.Err(err))
		return nil, err
	}

	if err := c.syncChartRepoSecret(body, old, result); err != nil {
		logger.Error("update secret error", log.Err(err))
		return nil, err
	}

	return result, nil

}

func (c *ComposedClient) removeChartRepoSecret(ns, name string) error {
	logger.Info("need to remove secret")
	if err := c.setCoreClient(); err != nil {
		logger.Error("create client error", log.Err(err))
		return err
	}
	return c.coreClient.CoreV1().Secrets(ns).Delete(name, &metav1.DeleteOptions{})

}

func (c *ComposedClient) updateChartRepoSecret(body *models.ChartRepoUpdate, owner metav1.Object) error {
	logger.Info("need to update secret")

	if err := c.setCoreClient(); err != nil {
		logger.Error("create core client error", log.Err(err))
		return err
	}

	body.Spec.Secret.SetOwnerReferences([]metav1.OwnerReference{
		*util.NewOwnerRef(owner, schema.GroupVersionKind{
			Group:   "app.alauda.io",
			Version: "v1beta1",
			Kind:    "ChartRepo",
		}),
	})

	secret, err := c.coreClient.CoreV1().Secrets(body.GetNamespace()).Update(body.Spec.Secret)
	if err != nil {
		if apierrors.IsNotFound(err) {
			logger.Info("secret for chartrepo not found, create it")
			_, err = c.coreClient.CoreV1().Secrets(body.GetNamespace()).Create(body.Spec.Secret)
			if err != nil {
				logger.Error("create secret error", log.Err(err))
				return err
			}
		} else {
			logger.Error("update secret error", log.Err(err))
			return err
		}
	}
	logger.Info("update secret", log.Any("secret", secret))
	return nil
}

// createChartRepo create chartrepo and secret
func (c *ComposedClient) createChartRepo(body *models.ChartRepoCreate) (*v1beta1.ChartRepo, error) {
	body.Spec.ChartRepo.Status.Phase = v1beta1.ChartRepoPending

	// create secret first to avoid hr was synced too quickly
	if body.Spec.Secret != nil {
		logger.Info("need to create secret")
		if err := c.setCoreClient(); err != nil {
			logger.Error("create core client error", log.Err(err))
			return nil, err
		}

		secret, err := c.coreClient.CoreV1().Secrets(body.GetNamespace()).Create(body.Spec.Secret)
		if err != nil {
			logger.Error("create secret error", log.Err(err))
			return nil, err
		}
		logger.Info("create secret", log.Any("secret", secret))
	}

	// create chartrepo now.
	result, err := c.appClient.AppV1beta1().ChartRepos(body.GetNamespace()).Create(body.Spec.ChartRepo)
	if err != nil {
		logger.Error("create repo error", log.Err(err))
		return nil, err
	}
	logger.Info("create chartrepo", log.String("name", result.GetName()))

	// patch ownerReferences now
	if body.Spec.Secret != nil {
		logger.Info("patch secret to add owner ref")
		pd := fmt.Sprintf(`{"metadata":{"ownerReferences":[{"apiVersion":"app.alauda.io/v1beta1","blockOwnerDeletion":true,"controller":false,"kind":"ChartRepo","name":"%s","uid":"%s"}]}}`, result.GetName(), string(result.GetUID()))
		secret, err := c.coreClient.CoreV1().Secrets(body.GetNamespace()).Patch(body.Spec.Secret.GetName(), types.StrategicMergePatchType, []byte(pd))
		if err != nil {
			logger.Error("update secret owner ref error", log.Err(err))
			return nil, err
		}
		logger.Info("update secret", log.Any("secret", secret))
	}

	return result, nil
}

// CreateChartRepo create a models and it's secrets if have
func (h *Handler) CreateChartRepo(req *restful.Request, res *restful.Response) {
	cfg := wrapper.Config(req.Request.Context())
	logger.Debug("rest config is: ", log.Any("config", cfg))

	var body models.ChartRepoCreate
	if err := req.ReadEntity(&body); err != nil {
		logger.Error("read request body error", log.Err(err))
		h.Server.HandleError(err, req, res)
		return
	}

	if body.Spec.ChartRepo == nil {
		logger.Error("body not contains chartrepo")
		h.Server.HandleError(apierrors.NewBadRequest("body not contains chartrepo resource"), req, res)
		return
	}

	c, err := getComposedClient(req)
	if err != nil {
		logger.Error("create client error", log.Err(err))
		h.Server.HandleError(err, req, res)
		return
	}

	result, err := c.createChartRepo(&body)
	if err != nil {
		logger.Error("create repo error", log.Err(err))
		h.Server.HandleError(err, req, res)
		return
	}
	logger.Info("create models", log.String("models", result.GetName()))

	res.WriteAsJson(result)

	return
}
