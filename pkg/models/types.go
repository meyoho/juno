package models

import (
	"github.com/alauda/helm-crds/pkg/apis/app/v1beta1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
)

type ChartRepoCreate struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec ChartRepoCreateSpec `json:"spec"`
}

type ChartRepoCreateSpec struct {
	ChartRepo *v1beta1.ChartRepo `json:"chartRepo, omitempty"`
	Secret    *v1.Secret         `json:"secret,omitempty"`
}

type ChartRepoUpdate ChartRepoCreate

type ChartVersion struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec v1beta1.ChartVersion `json:"spec,omitempty"`
}

// ChartFiles contains files information of a chart
type ChartFiles struct {
	Files map[string]string `json:"files"`
}

type ChartResources struct {
	Resources []unstructured.Unstructured `json:"resources"`
}
