package models

import "k8s.io/client-go/rest"

// ConfigForCluster gen a rest config for business clusters
func ConfigForCluster(cfg *rest.Config, cluster string) *rest.Config {
	return &rest.Config{
		Host:        "https://erebus:443/kubernetes/" + cluster,
		BearerToken: cfg.BearerToken,
		TLSClientConfig: rest.TLSClientConfig{
			Insecure: true,
		},
	}
}
