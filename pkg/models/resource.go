package models

import (
	"bytes"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/util/yaml"
	"strings"
)

// ParserChartResources parse resources list from manifest string in Release
func ParseChartResources(manifest string) (*ChartResources, error) {
	var rs []unstructured.Unstructured

	ss := strings.Split(manifest, "---")
	for _, item := range ss {
		if item == "" {
			continue
		}
		var res unstructured.Unstructured
		d := yaml.NewYAMLOrJSONDecoder(bytes.NewBufferString(item), len([]byte(item)))
		err := d.Decode(&res)

		// err := yaml.Unmarshal([]byte(item), &res)
		if err != nil {
			return nil, err
		}
		rs = append(rs, res)
	}

	return &ChartResources{Resources: rs}, nil

}
