package kubernetes

import (
	"encoding/json"
	"github.com/alauda/helm-crds/pkg/apis/app/v1alpha1"
	"github.com/gsamokovarov/assert"
	"io/ioutil"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"testing"
)

func loadChartRepo(path string) *v1alpha1.ChartRepo {
	plan, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}
	var data v1alpha1.ChartRepo
	if err := json.Unmarshal(plan, &data); err != nil {
		panic(err)
	}
	return &data
}

func TestSetOwner(t *testing.T) {
	cr := loadChartRepo("../../test/fixtures/chartrepo.json")
	result := NewOwnerRef(cr, schema.GroupVersionKind{
		Group:   "app.alauda.io",
		Version: "v1alpha1",
		Kind:    "ChartRepo",
	})

	assert.Equal(t, result.APIVersion, "app.alauda.io/v1alpha1")
	assert.Equal(t, result.Name, cr.GetName())
	assert.Equal(t, result.UID, cr.GetUID())

}
