package helm

import (
	"alauda.io/juno/pkg/models"
	"archive/tar"
	"bytes"
	"compress/gzip"
	"fmt"
	"github.com/patrickmn/go-cache"
	"github.com/pkg/errors"
	"helm.sh/helm/pkg/chart/loader"
	"helm.sh/helm/pkg/repo"
	"io"
	"io/ioutil"
	"k8s.io/klog"
	"net/http"
	"strings"
	"time"
)

var (
	chartsCache = cache.New(3*time.Minute, 5*time.Minute)
)

// DownloadChart download a chart
func DownloadChart(entry *repo.Entry, url string) (*models.ChartFiles, error) {
	data, err := downloadFile(entry, url)
	if err != nil {
		return nil, err
	}
	return loadArchive(data)
}

// downloadFile download charts data to memory and cached it
func downloadFile(entry *repo.Entry, url string) (io.Reader, error) {
	// Get the data
	if !strings.HasPrefix(url, "http") {
		if strings.HasSuffix(entry.URL, "/") {
			url = fmt.Sprintf("%s%s", entry.URL, url)
		} else {
			url = fmt.Sprintf("%s/%s", entry.URL, url)
		}

	}

	klog.Info("request chart url: ", url)

	result, found := chartsCache.Get(url)
	if found {
		klog.Info("found chart data in cache: ", url)
		respBytes := result.([]byte)
		return bytes.NewReader(respBytes), nil
	}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	if entry.Username != "" {
		req.SetBasicAuth(entry.Username, entry.Password)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	chartsCache.Set(url, respBytes, cache.DefaultExpiration)

	return bytes.NewReader(respBytes), nil
}

// loadArchive loads from a reader containing a compressed tar archive.
// copy from helm
func loadArchive(in io.Reader) (*models.ChartFiles, error) {
	unzipped, err := gzip.NewReader(in)
	if err != nil {
		return nil, err
	}
	defer unzipped.Close()

	files := []*loader.BufferedFile{}
	tr := tar.NewReader(unzipped)
	for {
		b := bytes.NewBuffer(nil)
		hd, err := tr.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}

		if hd.FileInfo().IsDir() {
			// Use this instead of hd.Typeflag because we don't have to do any
			// inference chasing.
			continue
		}

		// Archive could contain \ if generated on Windows
		delimiter := "/"
		if strings.ContainsRune(hd.Name, '\\') {
			delimiter = "\\"
		}

		parts := strings.Split(hd.Name, delimiter)
		n := strings.Join(parts[1:], delimiter)

		// Normalize the path to the / delimiter
		n = strings.ReplaceAll(n, delimiter, "/")

		if parts[0] == "Chart.yaml" {
			return nil, errors.New("chart yaml not in base directory")
		}

		if _, err := io.Copy(b, tr); err != nil {
			return nil, err
		}

		files = append(files, &loader.BufferedFile{Name: n, Data: b.Bytes()})
		b.Reset()
	}

	return generateChartFiles(files)
}

func generateChartFiles(files []*loader.BufferedFile) (*models.ChartFiles, error) {
	if len(files) == 0 {
		return nil, errors.New("no files in chart archive")
	}
	result := make(map[string]string)
	for _, item := range files {
		result[item.Name] = string(item.Data)
	}
	return &models.ChartFiles{
		Files: result,
	}, nil
}
