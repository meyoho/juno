package helm

import (
	"bitbucket.org/mathildetech/log"
	"github.com/gsamokovarov/assert"
	"go.uber.org/zap"
	"helm.sh/helm/pkg/repo"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

type downloadHandler struct{}

func (h *downloadHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	logger := zap.NewExample()

	data, err := ioutil.ReadFile("../../test/fixtures/captain-v0.9.2.tgz")
	if err != nil {
		panic(err)
	}
	if count, err := w.Write(data); err != nil {
		panic(err)
	} else {
		logger.Info("write data length", log.Int("len", count))
	}

}

func TestDownloader(t *testing.T) {
	h := downloadHandler{}
	server := httptest.NewServer(&h)
	defer server.Close()

	entry := repo.Entry{}

	result, err := DownloadChart(&entry, server.URL)
	assert.Nil(t, err)
	_, ok := result.Files["Chart.yaml"]
	assert.True(t, ok)

}
