FROM alpine:3.11

COPY output /tmp/output

ENV TZ=Asia/Shanghai
ARG commit_id=dev
ARG app_version=dev

WORKDIR /console

CMD ["/console/juno"]

ENTRYPOINT ["/bin/sh", "-c", "/console/juno \"$0\" \"$@\""]

# pick the architecture
RUN ARCH= && dpkgArch="$(arch)" \
  && case "${dpkgArch}" in \
    x86_64) ARCH='amd64';; \
    aarch64) ARCH='arm64';; \
    *) echo "unsupported architecture"; exit 1 ;; \
  esac \
  && cp /tmp/output/linux/$ARCH/juno /console/juno
 
# ADD output/linux/amd64/juno /console/juno

ENV COMMIT_ID=${commit_id}
ENV APP_VERSION=${app_version}
